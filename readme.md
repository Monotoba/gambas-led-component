# GAMBAS LED Component

An LED component that can be assigned different shapes and colors. 
I developed this for an embedded project. However, it also conjures 
up ideas of creating an Altair 8800 or Kenbak Simulator. 

![picture](pictures/screen-shot-led-component.png)

Enjoy!